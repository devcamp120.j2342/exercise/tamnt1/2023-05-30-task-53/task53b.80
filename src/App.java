import model.Circle;
import model.Shape;
import model.Square;
import model.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println(shape1 + "" + shape2);
        Circle cirle1 = new Circle();
        Circle cirle2 = new Circle(3.0);
        Circle cirle3 = new Circle(3.0, "green", true);

        System.out.println(cirle1 + "" + cirle2 + "" + cirle3);

        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle("green", true, 2.5, 1.5);

        System.out.println(rectangle1 + "" + rectangle2 + "" + rectangle3);

        System.out.println(rectangle1.getArea() + "" + rectangle2.getArea() + "" + rectangle3.getArea());

        System.out.println(rectangle1.getPerimeter() + "" + rectangle2.getPerimeter() + "" + rectangle3.getPerimeter());
        Square square1 = new Square();
        Square square2 = new Square(1.5);

        System.out.println(square1 + "" + square2);
    }
}
